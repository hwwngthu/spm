<%-- 
    Document   : setting
    Created on : May 14, 2022, 4:49:52 PM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Setting List</title>
    </head>
    <body>
        <table border="1px">
            <thead>
            <th>Setting</th>
            <th style= "margin-left: auto; margin-right: auto">Activate/Deactivate</th>
        </thead>
        <tbody>
            <tr>
                <td>Setting 1</td>
                <td><input type="checkbox" style="margin-left: auto; margin-right: auto"></td>
                
            </tr>
            <tr>
                <td>Setting 2</td>
                <td><input type="checkbox" style="margin-left: auto; margin-right: auto"></td>
            </tr>
        </tbody>
    </table>
        <button><a href="">Setting Details</a></button>
</body>
</html>
